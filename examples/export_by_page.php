<?php

require_once('../bookstack_client.php');
require_once('./credentials.php');

if (!isset($credentials)) {
    die("Missing `credentials` array; exiting now.");
}

$europa = new BookStack_Client($credentials['url'], $credentials['id'], $credentials['secret'], true);
// Set a custom path - uncomment below
// $europa->update_output_path("/test/");

// Export all pages
// Uses book slug name as sub-directory for each page.
$europa->export_all_pages();
