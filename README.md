# BookStack Utilities - PHP

Ideal for use cases:
- Import static HTML content in to BookStack.
- Export BookStack content as books (_combined_) or pages (_separated_).

## History
This project was originally put together to aid in migrating content from Atlassian Confluence to BookStack. 

It requires that the Confluence Space to be imported is first exported to HTML. Then that HTML can be imported to BookStack using this API client helper library.

The requirements were simplistic for the import task; the importer in its default state will create a Book using the name you provide and the import HTML files from the path provided. Each HTML document imported will create a new page within the book using the file name as the page name.

Feel free to adapt this to your own requirements.

**Notice**: this doesn't handle attachments.

## Usage Instructions

### Authentication

To run an example; create a credentials file: `credentials.php` and include your Rest API credentials from BookStack.

```php
$credentials = array(
	'url' => "<bookstack-path>",    // Base url for Bookstack location
	'id' => "<id>",                 // Token ID
	'secret' => "<api-token>"       // Token Secrete
);
```

### Import statuc HTML
See `examples/importer.php` for a working example.

Essentially you want to do the following:
- Exported a space to HTML.
- Extract the space and copy the path to the location.
- Define a Book name and include the book name and imported path for the HTML in the file: `importer.php` lines: 13-14.
- Run the import task; which will create a Book and imported each HTML page as a page with the Book.

### Export all books as Markdown file
Iterates overs available books to the users and export them as Markdown.

Each book is a separate file.

See `examples/export_by_book.php` for a working example.

### Export all pages as Markdown file
Iterates overs available pages to the users and export them as Markdown.

Each page is stored in a sub-directory derived from the book slug name.

See `examples/export_by_page.php` for a working example.
