<?php
/*
 * Copyright (c) 2022. Oddineers Ltd. All Rights Reserved
 * License: MIT License
 * Written by Steven Brown <mortanius@oddineers.co.uk>, 2022
 */

class BookStack_Client {
	public $domain_url = false;
	public $url = false;
	public $last_url = false;
	public $last_status_code = false;
	public $last_status_message = false;
	private $headers = array();
	private $debug = false;
	private $cookie = false;
	protected $data_string = false;
	private $file_as_json = false;
    private $c_types = array(
		"gif" => "image/gif",
		"jpg" => "image/jpeg",
		"png" => "image/png",
		"pdf" => "application/pdf",
		"txt" => "text/plain",
	);

    private $extension_format = [
        'pdf' => 'pdf',
        'html' => 'html',
        'plaintext' => 'txt',
        'markdown' => 'md',
    ];

    private $output_dir = null;

    // Cache book ID's: "id" => "name"
    private $cached_book_ids = array();

	function __construct( $domain_url, $id, $secret, $debug = false  ) {
		if ( empty( $domain_url ) ) {
			$this->last_status_message = 'The hostname/url cannot be empty.';

			return false;
		}
		if ( empty( $id ) ) {
			$this->last_status_message = 'The authenticating username cannot be empty.';

			return false;
		}
		if ( empty( $secret ) ) {
			$this->last_status_message = 'The users application password cannot be empty.';

			return false;
		}

		$this->domain_url = $domain_url;
		$prefix           = "/api/";

		if ( is_string( $domain_url ) && $this->ends_with( $domain_url, "/" ) ) {
			$prefix = "api/";
		}

		$this->url              = "{$domain_url}{$prefix}";
		$this->last_url         = null;
		$this->last_status_code = null;

		# Setup debug flag for printing results
		if ( $debug ) {
			$this->debug = true;
		} else {
			$this->debug = false;
		}

		$this->data_string = "{$id}:{$secret}";
        $this->output_dir = realpath('./docs/');
	}

    /**
     * Assign a custom output path.
     * @param $output_dir
     * @return bool
     */
    function update_output_path($output_dir) {
        // Assign custom directory if overridden and exists
        if (is_dir( $output_dir )) {
            $this->output_dir = $output_dir;
            return true;
        }
        return false;
    }

	/**
	 * Basic authentication
	 * @return void
	 */
	protected function basic_auth() {
		//$encoded_token = base64_encode( $this->data_string );
		$encoded_token = $this->data_string;
		$this->headers = array_merge( array(
			'Authorization: Token ' . $encoded_token, // utf8_decode( $encoded_token ),
		), $this->headers );
	}

	/**
	 * HTTP based authetnication with Cookie Jar.
	 *
	 * @param $ch
	 *
	 * @return void
	 */
    protected function user_auth( $ch ) {
        //$encoded_token = base64_encode( $this->data_string );
        $encoded_token = $this->data_string;
        curl_setopt( $ch, CURLOPT_USERPWD, $encoded_token );
        curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
        //curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt( $ch, CURLOPT_COOKIEJAR, $this->cookie );
        curl_setopt( $ch, CURLOPT_COOKIEFILE, $this->cookie );
    }

	/**
	 * Dumps values if debugging is enabled.
	 *
	 * @param $string
	 *
	 * @return bool
	 */
	function log( $string ) {
		if ( $this->debug ) {
			var_dump( $string );

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Create a POST request; supports a variety of scenarios.
	 *
	 * @param $url
	 * @param array $payload
	 * @param $type
	 * @param $filename
	 *
	 * @return bool|string
	 */
	function post($url, $payload = array(), $type = "POST", $filename = false ) {
		// Reset header
		$this->headers = array();
		// Set the last url attribute to match this request
		$this->last_url = $url;
		// Get cURL resource
		$curl = curl_init();

		// Custom POST/GET requests
		if ( $type === "POST" ) {
			curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );
            curl_setopt( $curl, CURLOPT_POSTFIELDS, $payload );
		}

		if ( $type === "GET" ) {
			curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'GET' );
		}

		if ( $type === "PUT" ) {
			curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'PUT' );
		}

		// If file is empty set default header to JSON
		if ( $type === "POST" && $filename === false ) {
			$payload       = json_encode( $payload );
			$this->headers = array_merge( $this->headers, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen( $payload )
			) );
		} // No file assume JSON based request
		else if ( is_string( $filename ) && file_exists( $filename ) ) {
			$base_filename = basename( $filename );
			$path_parts    = pathinfo( $filename );
			$ext           = $path_parts['extension'];

			// If the content type is valid
			if ( isset( $this->c_types[ $ext ] ) ) {
				// File sent as JSON parameter
				if ( $this->file_as_json ) {
					$this->headers = array_merge( array(
						'Content-Type: application/json;',
					), $this->headers );

					$file_payload = new CURLFile( $filename, $this->c_types[ $ext ], $base_filename );
					$file_payload = array( 'file' => $file_payload ); //new CURLFILE($filename)); //$payload);

					$payload             = array_merge( $payload, $file_payload );
					$payload['contents'] = utf8_encode( file_get_contents( $filename ) );
					$payload             = json_encode( $payload );

					$this->log( $payload );
				} // Otherwise sent as attachment file content type
				else {
					// Attachment header
					$this->headers = array_merge( array(
						"Content-Disposition: attachment; filename='{$base_filename}'; charset=utf-8"
					), $this->headers );
					// Content type derived from extension
					$this->headers = array_merge( array(
						"Content-Type: {$this->c_types[$ext]}; charset=utf-8"
					), $this->headers );
				}
			}
		}

		// Auth method
		$this->basic_auth();
		//$this->user_auth($curl);

		// Set some options - we are passing in a useragent too here
		curl_setopt( $curl, CURLOPT_URL, $url );
		curl_setopt( $curl, CURLOPT_HTTPHEADER, $this->headers );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $curl, CURLOPT_USERAGENT, 'europa_bookstack' );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

        // Send the request & save response to $resp
		$resp      = curl_exec( $curl );
		$http_code = curl_getinfo( $curl, CURLINFO_HTTP_CODE );
		$errors    = curl_error( $curl );
		curl_close( $curl );

		$this->last_status_message = $errors;
		$this->last_status_code    = $http_code;

		return ( $http_code >= 200 && $http_code < 300 ) ? $resp : $errors;
	}

	/**
	 * Utilises `post` to make a GET request using similar options.
     *
	 * @param $url
	 * @param array $payload
	 *
	 * @return bool|string
	 */
	function get($url, $payload = array() ) {
		return $this->post( $url, $payload, "GET" );
	}

	/**
	 * Utilises `post` to make a PUT request using similar options.
     *
	 * @param $url
	 * @param $payload
	 *
	 * @return bool|string
	 */
	function put( $url, $payload ) {
		return $this->post( $url, $payload, "PUT" );
	}

	/**
	 * Retrieves a list of shelves.
	 * @param $payload
	 *
	 * @return false
	 */
	function get_shelves( $payload ) {
		if ( is_array( $payload ) ) {
			$shelves = $this->get( "{$this->url}shelves", $payload );
			if ( isset( $shelves ) ) {
				$shelves = json_decode( $shelves );
				if ( isset( $shelves->data ) ) {
					$shelves = $shelves->data;
					$this->log( $shelves );

					return $shelves;
				}
			}
		}

		return false;
	}

    /**
     * GET Helper function to list from selected types: books, pages
     *
     * @param $type book or page
     * @param $payload
     * @return false|mixed
     */
    protected function get_thing_helper($type, $payload = array() ) {
        $url = "{$this->url}{$type}";

        if ( !empty( $payload ) ) {
            $url = "{$this->url}{$type}?" . http_build_query($payload);

            if (isset($payload["id"])) {
                $url = "{$this->url}{$type}/{$payload["id"]}";
            }
        }

        $books = $this->get($url);
        //$this->log( $books );

        if ( isset( $books ) ) {
            $books = json_decode( $books );
            if (!empty($books)) {
                return $books;
            }
        }

        return false;
    }

    /**
     * Creates a files from data; helper functions to be used by _export_thing_paginated_ with items such as books, pages.
     *
     * @param $type book or page
     * @param $data data from a
     * @param $ext
     * @return void
     */
    protected function create_file($type, $data, $ext = 'markdown') {
        if ($data) {
            // iterate over books and save each entry
            foreach ($data as $entry) {
                if (!isset($entry->slug)) {
                    continue;
                }

                $id = $entry->id;
                $extension = $this->extension_format[$ext];
                $content = $this->get("{$this->url}{$type}/{$id}/export/{$ext}");
                //$this->log(array($type, $entry));
                $slug = "{$entry->slug}";
                $outpath = $this->output_dir;

                // Change pages to use the book slug as a sub-directory
                if ($type === "pages") {
                    if (isset($entry->book_id)) {
                        $book_name = $this->get_book_name($entry->book_id);

                        if (empty($book_name)) {
                            $book_name = "unknown";
                        }
                        $outpath = "{$this->output_dir}/{$book_name}";

                        // Dir doesn't exist make it
                        if (!is_dir($outpath)) {
                            mkdir($outpath);
                        }
                    }
                }

                $out = "{$outpath}/{$slug}.{$extension}";
                $this->log($out);
                file_put_contents($out, $content);
            }
        }
    }

    /**
     * Used with pagination functionality; changes the type (book or page) and sets the count and offset
     * @param $type
     * @param $count
     * @param $offset
     * @return false|mixed
     */
    protected function set_type($type, $count, $offset) {
        switch ($type) {
            case "pages":
                $data = $this->get_pages($count, $offset);
                break;
            default:
                $data = $this->get_books($count, $offset);
                break;
        }

        return $data;
    }

    /**
     * Export *things*; helper functions to be used by types such as books, pages.
     *
     * @param $type
     * @return void
     */
    protected function export_thing_paginated($type) {
        $count = 100;
        $processed = 0;

        // Set the data type (books or pages)
        $data = $this->set_type($type, $count, $processed);

        if ( isset( $data->total ) ) {
            $total = $data->total;

            while ($processed < $total) {
                // Get next set when processed > 0
                if ($processed > 0) {
                    //$this->log( array("new {$type} query", $count, $processed, $this->last_status_message) );
                    $data = $this->set_type($type, $count, $processed);
                }

                if ( isset( $data->data ) ) {
                    $data = $data->data;
                    $this->create_file($type, $data, 'markdown');
                }

                $processed += $count;
            }
        }
    }

    /**
     * Retrieve a list of *things*; helper functions to be used by types such as books, pages.
     *
     * @param $payload
     * @return false|mixed
     */
    protected function get_thing($type, $payload = array() ) {
		return $this->get_thing_helper($type, $payload);
	}

    /**
     * Get information about a book by book ID.
     *
     * @param $id
     * @return false|mixed
     */
    function get_book($id) {
        $book = $this->get_thing("books", ['id' => $id]);
        return $book;
    }

    /**
     * Retrieve a list of books based on a count + offset.
     * Defaults to 100, 0.
     *
     * @param $count
     * @param $offset
     * @return false|mixed
     */
    function get_books($count = 100, $offset = 0) {
        $books = $this->get_thing("books", ['count' => $count, 'offset' => $offset]);
        return $books;
    }

    /**
     * Get the book (slug) name by book id.
     *
     * @param $id
     * @return false
     */
    function get_book_name($id) {
        // Test cache first - less likely to hit rate limits
        if (isset($this->cached_book_ids[$id])) {
            return $this->cached_book_ids[$id];
        }

        // Query the book name (we use slug) from the ID
        $book = $this->get_book($id);

        if (isset($book->slug)) {
            $this->cached_book_ids[$id] = $book->slug;
            return $book->slug;
        }

        return false;
    }

	/**
	 * Creates a new book if it does not exists.
	 * $check_existing Default: true. When true checks if there is an existing book with the same name and returns
	 * that books ID, when false creates a new book regardless
	 *
	 * @param $book_title
	 * @param $check_existing
	 *
	 * @return false|mixed
	 */
	function create_book( $book_title, $check_existing = true ) {
		$book_id = false;

		# Check books if required (default)
		if ( is_bool( $check_existing ) && $check_existing ) {
			$books = $this->get_books( array() );

			if ( $books ) {
				foreach ( $books as $book ) {
					$this->log( $book->name );
					//continue;
					if ( $book->name === $book_title ) {
						$book_id = $book->id;
						$this->log( "Book exists with ID: {$book_id}" );

						return $book_id;
					}
				}
			}
		}

		# No book was found create one
		if ( $book_id === false ) {
			# Book doesn't exists so create it
			$payload  = array(
				"name"        => "{$book_title}",
				"description" => ""
			);
			$new_book = $this->post( "{$this->url}books", $payload );
			if ( isset( $new_book ) ) {
				$new_book = json_decode( $new_book );
				if ( isset( $new_book->id ) ) {
					$book_id = $new_book->id;
					$this->log( "Created Book with ID: {$book_id}" );
				}
			}
		}

		return $book_id;
	}

    /**
     * Export all books to a file.
     * @return void
     */
    function export_all_books() {
        $this->export_thing_paginated("books");
    }

    /**
     * Retrieve a page by ID.
     * @param $id
     * @return false|mixed
     */
    function get_page($id) {
        $page = $this->get_thing("pages", ['id' => $id]);
        return $page;
    }

    /**
     *  Retrieve a list of pages based on a count + offset.
     *  Defaults to 100, 0.
     * @param $count
     * @param $offset
     * @return false|mixed
     */
    function get_pages($count = 100, $offset = 0) {
        $pages = $this->get_thing("pages", ['count' => $count, 'offset' => $offset]);
        return $pages;
    }

    /**
     * Export all pages to files; uses the book slug name as sub-directory to store each page.
     * @return void
     */
    function export_all_pages() {
        $this->export_thing_paginated("pages");
    }

	/**
	 * Creates a page within a book.
	 *
	 * @param $payload array Requires keys: book_id, name, html
	 *
	 * @return false
	 */
	function create_page( $payload ) {
		// Must be an array and have the 3 keys
		if (
			is_array( $payload )
			&& isset( $payload['book_id'] )
			&& isset( $payload['name'] )
			&& isset( $payload['html'] )
		) {
			$page = $this->post( "{$this->url}pages", $payload );

			if ( isset( $page ) ) {
				$page = json_decode( $page );
				if ( isset( $page->data ) ) {
					$page = $page->data;
					$this->log( $page );

					return $page;
				}
			}
		}

		return false;
	}

    public function starts_with( $haystack, $needle ) {
		$length = strlen( $needle );
		return substr( $haystack, 0, $length ) === $needle;
	}

    public function ends_with($haystack, $needle ) {
		$length = strlen( $needle );
		if ( ! $length ) {
			return true;
		}

		return substr( $haystack, - $length ) === $needle;
	}
}
